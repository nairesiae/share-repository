#import necessary libraries
import datetime, pytz

#function to check if creation date of VM is within the last 7 days
def new_VMs(VM):
    #calculate date 7 days ago
    today = datetime.datetime.now()
    weekdelta = datetime.timedelta(days=7)
    lastweek = today - weekdelta

    #create datetime object from VM created value
    date_created = datetime.datetime.strptime(VM['created'].split('T')[0],'%Y-%m-%d')
    #return true if date created is greater than or equal to last week value
    age = check_date(date_created,lastweek )

    return age

#function to check if creation date of VM is within the last 30 days
def monthVMs(VM):
    #calculate date 30 days ago
    today = datetime.datetime.now()
    monthdelta = datetime.timedelta(days=30)
    lastmonth = today - monthdelta

    #create datetime object from VM created value
    date_created = datetime.datetime.strptime(VM['created'].split('T')[0],'%Y-%m-%d')
    #return true if date created is greater than or equal to last month value
    age = check_date(date_created, lastmonth )

    return age

#method that returns true if date_created is greater than or equal to date_delta
def check_date(date_created, date_delta):
    if date_created >= date_delta:
     return True

    return False

#method return list of accounts that contain search_value in their name
def search_list(domain_id, search_value):
    #prepare and make API query call
    query = {'domainid': domain_id}
    result = api.listAccounts(query)

    #create regex value that ignores case and contains fragment of a string
    regex = re.compile(".*"+re.escape(search_value)+".*", re.I))
    #extract list of accounts
    accounts = result['account']
  
    #create filtereed list of accounts in account list by doing a regex search in account names
    search_list = [account for account in accounts if regex.search(account['name'])]

    #return the filtered list
    return search_list
