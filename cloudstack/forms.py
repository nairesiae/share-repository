from django import forms
from choices import *

class SearchForm(forms.Form):
    your_search = forms.CharField(label='Account', max_length=100, required=True)
    domain = forms.ChoiceField(choices = DOMAIN_CHOICES, required=True)
