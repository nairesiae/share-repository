

# Create your views here.

from django.shortcuts import get_object_or_404, render
# import all the necessary classes/libraries
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse

from django.contrib.auth.decorators import login_required

#custom class to allow interaction with the Cloudstack API
from cloud_api import CloudStack

import re
from operator import itemgetter, attrgetter, methodcaller

#custom form class for use in the search accounts view function
from forms import SearchForm

#import constants
from constants import *

#import helper functions called by the view methods below
from helper_functions import *

# create an object of class Cloudstack to allow API calls to be made
api = CloudStack(api_url, apiKey, secret)

#home page
def index(request):
    return render(request, 'cloudstack/index.html')

#view below returns a list of all the domains
@login_required
def domains(request):
    #prepare and make API call
    query = {'listall':'true'}
    result = api.listDomains(query)
    
    #extract total number of VMs from the list of dictionaries returned
    total = result['count']
    #extract a list of the domains
    domains_list = result['domain']
    context = {'total': total, 'domains_list': domains_list}
    
    # pass on context info to the appropriate template for rendering
    return render(request, 'cloudstack/domains.html', context)

#view below returns a list of all acoounts in a domain
@login_required
def domain(request, domain_id):
    #prepare and make API call
    query = {'domainid': domain_id}
    result = api.listAccounts(query)
    
    #extract total number of accounts
    total = result['count']
    #extract a list of accounts
    accounts_list = result['account']
 
    #sort the account names alphabetically using python function sorted
    accounts_list = sored(accounts_list, key=lambda account: account['name'].lower()) 
   
    context = {'total': total, 'domain_id': domain_id, 'accounts_list': accounts_list}
   
    #pass on context info to template for rendering
    return render(request, 'cloudstack/domain.html', context)

#view that returns a list of all virtual machines (if any) in account
@login_required
def account(request, domain_id, account_name):
    #prepare and make API call
    query = {'account': account_name, 'domainid': domain_id}
    result = api.listVirtualMachines(query)
    
    #handle exception in the case there are no VMs
    try:
     #extract total number of VMs and a list of all VMs
     total = result['count']
     VMs_list = result['virtualmachine']
    
     context = {'account': account_name, 'domainid': domain_id, 'total': total, 'VMs_list': VMs_list}
    #pass on context info to template for rendering
    return render(request, 'cloudstack/account.html', context)
   
    except Exception:
    #if there are no VMs pass on total of 0
     total = 0

     context = {'account': account_name, 'domainid': domain_id, 'total': total}
     return render(request, 'cloudstack/account.html', context)

#view that returns VM details
@login_required
def VM(request, domain_id, account_name, VM_id):
    #prepare and make API call
    query = {'id': VM_id }
    result = api.listVirtualMachines(query)
    
   #extract the single virtual machine from the list of dictionaries
    virtual_machine = result['virtualmachine']
    virtual_machine = virtual_machine[0]
    
    #prepare and make API call to extract volumes (disks) corresponding to the VM id
    query2 = { 'listall': 'true', 'virtualmachineid': VM_id}
    result2 = api.listVolumes(query2)
    total = 0

    #catch exception in the event that there are no volumes attached to the VM
    try:
     #extract total number of volumes and a list of volumes 
     total = result2['count']
     volumes_list = result2['volume']
     context = {'account': account_name, 'total': total, 'volumes_list': volumes_list, 'virtual_machine': virtual_machine}
     #pass on info to template for rendering
     return render(request, 'cloudstack/VM.html', context)
 
    except Exception:
     #if there are no volumes pass total as 0
     total = 0
     context = {'account': account_name, 'total': total, 'volumes_list': volumes_list, 'virtual_machine': virtual_machine}
     #pass on info to template
     return render(request, 'cloudstack/VM.html', context)

#view that returns a list of VMs created in the last one week (7 days)
@login_required
def latest_VMs(request):
    #prepare and make API call
    query = {'listall': 'true' }
    result = api.listVirtualMachines(query)
    
    #extract list of virtual machines
    virtual_machines = result['virtualmachine']   
   
    #filter the list based on creation date of the VM
    latest_list = filter(new_VMs, virtual_machines) 
   
    #obtain the length of the filtered list
    total = len(latest_list)
    context = {'latest_list': latest_list, 'total': total}
    #pass on context info to template for rendering
    return render(request, 'cloudstack/latest_VMs.html', context) 

#view that retruns a list of VMs created in the past one month (30 days)
@login_required
def month_VMs(request):
    #prepare and make API call
    query = {'listall': 'true' }
    result = api.listVirtualMachines(query)

    #extract list of virtual machines
    virtual_machines = result['virtualmachine']

    #filter VMs by creation date
    latest_list = filter(monthVMs, virtual_machines)

    #obtain the length of the filtered list
    total = len(latest_list)
    context = {'latest_list': latest_list, 'total': total}
    #pass on context info to template for rendering
    return render(request, 'cloudstack/month_VMs.html', context)   
  
#view to search by full or partial account name if an account exists in a particular domain
@login_required
def search_form(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = SearchForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            #extract data from form
            search_value = form.cleaned_data['your_search']
            domain = form.cleaned_data['domain']
            #match domain selected to domain id in predefined domain dictionary 
            domain_id = domain_dict[domain]
            #call the search method to see if a match exists
            result = search_list(domain_id, search_value)
            #chack the length of the list returned
            total = len(result)
            context = {'total': total, 'domain_id': domain_id, 'accounts_list': result}
            #pass on context to appropriate template
            return render(request, 'cloudstack/domain.html', context)
            

    # if a GET (or any other method) create a blank form
    else:
        #create a form object
        form = SearchForm()
    #pass form object to the template for rendering
    return render(request, 'cloudstack/search.html', {'form': form})

#view that returns a list of all VMs in a particular domain
@login_required
def list_VMs(request, domain_id):
    #prepare and make API calls
    query = {'domainid':domain_id }
    result = api.listVirtualMachines(query)
    
    #catch exception if there are no VMs in the domain
    try:
     #extract list of VMs as well as number of VMs in the domain
     VM_list = result['virtualmachine']
     total = result['count']
    
     #sort the list alphabetically by VM name
     VM_list = sorted(VM_list, key=lambda VM: VM['displayname'].lower()) 
     context = {'VM_list': VM_list, 'total': total}
     #pass on template info to context for rendering
     return render(request, 'cloudstack/list_VMs.html', context)

    except Exception:
    #if there are no VMs set total to 0
     VM_list = []
     total = 0

     context = {'VM_list': VM_list, 'total': total}

     #pass context info to template for rendering
     return render(request, 'cloudstack/list_VMs.html', context)


