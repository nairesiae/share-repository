
from django.conf.urls import url

from . import views
#import library to allow caching
from django.views.decorators.cache import cache_page

#specify application name
app_name = 'cloudstack'

#some pages below are cached
urlpatterns = [
    url(r'^$', views.index, name='index'),#default view
    url(r'^domains/$', cache_page(60 * 60)(views.domains), name='domains'),#display domains
    url(r'^latestvms/$', cache_page(60 * 15)(views.latest_VMs), name='latestVMs'),#display VMs created in the last 7 days
    url(r'^monthvms/$', cache_page(60 * 15)(views.month_VMs), name='monthVMs'),#display VMs created in the past one month
    url(r'^search/$', views.search_form, name='search_form'),#display the account search form
    url(r'^vms/(?P<domain_id>.*)/$', cache_page(60 * 15)(views.list_VMs), name='listVMs'),#list all VMs in a domain
    url(r'^domains/(?P<domain_id>.*)/(?P<account_name>.*)/(?P<VM_id>.*)/$', views.VM, name='VM'),#display VM info
    url(r'^domains/(?P<domain_id>.*)/(?P<account_name>.*)/$', cache_page(60 * 15)(views.account), name='account'),#display VMs in an account
    url(r'^domains/(?P<domain_id>.*)/$', cache_page(60 * 15)(views.domain), name='domain'), #display accounts in a domain
]
