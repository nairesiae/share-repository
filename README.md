# README #

### What is this repository for? ###

* Cloudstack Dashboard to help with collection of up to date billing info

### How do I get set up? ###

* Install Python 2.7/3.0 (This project was set up in a python 2.7 environment)
* Install pip
* Install virtualenv
* Install Django
* No Database configuration required
* Dependencies - install django-bootstrap3, mathfilter, pytz
* Deployment instructions (test environment) 
  - under project2 in settings.py specify the allowed_hosts as your VM IP - only for testing purposes
  - in the directory with all the files created run the command python manage.py runserver ipaddress:portnumber

### Structure guidelines ###

* cloudstack/ (django app)
  * cloud_api.py - provides the basic functionality to make api calls to cloudstack using python (provided on the Cloudstack API  site)
  * views.py - handles requests to different pages
  * urls.py - defines the different urls and their matching view functions
* cloudstack/templates/cloudstack - .html files display data in a desired format based on what is sent from the views
* users/ (django app) - handles login, registration, logout
* project2/ (project settings)
  * urls.py - specifies root url patterns that are expected in apps that are part of this project
  * settings.py - basic configs for the django apps in this project (any additional python modules installed must be specified    here under installed_apps)